output "log_alert_resource" {
  description = "Alert, log and Matrix have been enabled to following resource"
  value       = var.resource_id
}

output "log_target_location" {
  description = "Log storage destination ID"
  value       = var.logs_destinations_id
}