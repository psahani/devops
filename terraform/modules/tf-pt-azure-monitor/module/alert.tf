resource "azurerm_monitor_action_group" "monitor_action_group" {
  name                = "${var.alert_name}-ag"
  resource_group_name = var.rg_name
  short_name          = var.alert_name

  dynamic "email_receiver" {
    for_each = var.emails
    content {
      name                    = email_receiver.value.name
      email_address           = email_receiver.value.email_address
      use_common_alert_schema = email_receiver.value.use_common_alert_schema  
    }
  }
}

resource "azurerm_monitor_metric_alert" "az_alert" {
  name                = "${var.alert_name}-alert"
  resource_group_name = var.rg_name
  scopes              = ["${var.resource_id}"] 
  description         = var.alert_description

  criteria {
    metric_namespace = var.alert_namespace
    metric_name      = var.alert_metric_name
    aggregation      = var.alert_aggregation
    operator         = var.alert_operator
    threshold        = var.alert_threshold

    dimension {
      name     = "ApiName"
      operator = "Include"
      values   = ["*"]
    }
  }
   action {
          action_group_id = azurerm_monitor_action_group.monitor_action_group.id 
        }
}
