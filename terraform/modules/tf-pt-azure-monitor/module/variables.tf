
variable "alert_name" {
  type        = string
  default     = "default"
  description = "This name will be used to create the alert"
}

variable "rg_name" {
  type        = string
  default     = "default"
  description = "Resource group name of the resource"
}

variable "alert_description" {
  type        = string
  default     = "Action will be triggered  when transaction count is grater than defined threshold value"
  description = "Description to be used for creating alert."
}

variable "alert_namespace" {
  type        = string
  default     = "microsoft.storage/storageAccounts"
  description = "Provide the correct namespace to create alert."
}

variable "alert_metric_name" {
  type        = string
  default     = "Transactions"
}

variable "alert_aggregation" {
  type        = string
  default     = "Total"
}

variable "alert_operator" {  
  type        = string
  default     = "GreaterThan"
  }

variable "alert_threshold" {  
  type        = string
  default     = 50
  description = "Define the threshold for the alert"
  }

variable "emails" {
  description = "List of email receivers"
  type = list(object({
    name                    = string
    email_address           = string
    use_common_alert_schema = bool
  }))
  default = []
}

variable "resource_id" {
  type        = string
  description = "The ID of the resource on which activate the diagnostic settings."
}

variable "log_name" {
  type        = string
  default     = "default"
  description = "The name of the diagnostic setting."
}

variable "log_categories" {
  type        = list(string)
  default     = null
  description = "List of log categories."
}

variable "metric_categories" {
  type        = list(string)
  default     = null
  description = "List of metric categories."
}

variable "retention_days" {
  type        = number
  default     = 30
  description = "The number of days to keep diagnostic logs."
}

variable "logs_destinations_id" {
  type        = string
  description = "List of destination resources IDs for logs diagnostic destination. Can be Storage Account, Log Analytics Workspace and Event Hub. No more than one of each can be set."
}
