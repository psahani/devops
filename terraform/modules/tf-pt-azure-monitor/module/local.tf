locals {
  enabled               = length(var.logs_destinations_id) > 0

  log_categories = (
    var.log_categories != null ?
    var.log_categories :
    try(data.azurerm_monitor_diagnostic_categories.main[0].logs, [])
  )
  metric_categories = (
    var.metric_categories != null ?
    var.metric_categories :
    try(data.azurerm_monitor_diagnostic_categories.main[0].metrics, [])
  )

  logs = {
    for category in local.log_categories : category => {
      enabled        = true
      retention_days = var.retention_days
    }
  }

  metrics = {
    for metric in local.metric_categories : metric => {
      enabled        = true
      retention_days = var.retention_days
    }
  }

}