
provider "azurerm" {
  features {}
}

module "diagnostic-settings" {
        source  = "../module/"
        rg_name =  "dev"
        resource_id = "/subscriptions/d9cd9d34-5369-4594-910c-3ff4fa5af821/resourceGroups/dev/providers/Microsoft.Network/networkSecurityGroups/terraformvm-nsg"
        logs_destinations_id = "/subscriptions/d9cd9d34-5369-4594-910c-3ff4fa5af821/resourceGroups/dev/providers/Microsoft.Storage/storageAccounts/devguestdiag869"
        retention_days = 30
        alert_threshold = 50
        alert_namespace = "Microsoft.Network/networkSecurityGroups"
}

