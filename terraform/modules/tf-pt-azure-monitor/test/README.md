# tf-pt-azure-monitor


This module will create an azure monitoring alert, log & Matrics of given resources. 

## Introduction

The requirement is to create the alert, enable the log/matric of a given resource ID. As soon as the logs and matric are enabled, the logs/matrics data get stored under a defined storage account. 



## Requirements

| Name      | Version   |
| --------- | --------- |
| terraform | >= 0.13   |
| azurerm   | >= 2.16.0 |
| infoblox  | ~> 1.0    |
| restapi   | 1.14.1    |
| panos     | 1.8.3     |

## Dependencies

None



## Usage Example

```terraform

module "diagnostic-settings" {
        source  = "../module/"
        rg_name =  "dev"
        resource_id = "/subscriptions/d9cd9d34-5369-4594-910c-3ff4fa5af821/resourceGroups/dev/providers/Microsoft.Network/networkSecurityGroups/terraformvm-nsg"
        logs_destinations_id = "/subscriptions/d9cd9d34-5369-4594-910c-3ff4fa5af821/resourceGroups/dev/providers/Microsoft.Storage/storageAccounts/devguestdiag869"
        retention_days = 30
        alert_threshold = 50
        alert_namespace = "Microsoft.Network/networkSecurityGroups"
}
```

## Modules

This module is to create an alert, enable log and matric of any given resource.

`Alert`: As per the defined condition, the terraform module creates the alert of the given resource. 

`Log/Matric`: if the value for variable `log_categories` and `metric_categories` is not provided, it checks `log_catagories` and `metric_categories` of the given resource automatically and enable them. 

### Mendatory Inputs

| Name        | Description                                                  | Type          | Default | Required |
| ----------- | ------------------------------------------------------------ | ------------- | ------- | :------: |
| resource_id | Provide the complete resource ID.                | `string`        | [] |    yes    |
| logs_destinations_id    | Provide the storage account ID to store log.            | `string`      | []     |   yes    |
| rg_name        | Name of the resource group. | `string`      | []    |   yes    |
| alert_namespace        | Provide the namespace of the resource of which you want to create log/matric.     | `string` | []    |    yes    |

---



<br/>

### Alert, log, metric

#### Inputs

| Name                     | Description                                                     | Type     | Default             | Required |
|--------------------------|-----------------------------------------------------------------|----------|---------------------|:--------:|
| rg_name                | Resource group name of the resource.             | `string` | `"default"` | yes    
| resource_id         | Provide the complete path of the resource of which alert, log or matric needs to be created.            | `string`    | `[]`   | yes    
| logs_destinations_id         | Provide id of the storage account to be used to store log/matric.  | `string`    | `[]`   | yes      |
| alert_name | This name will be used to create the alert. | `string` | `"default"`           | no       |
 |
| alert_description         | Add description for alert.            | `string`    | `Action will be triggered  when transaction count is grater than defined threshold value`                 | no      |
| alert_namespace         | Provide the correct namespace to create alert.            | `string`    | `microsoft.storage/storageAccounts`                 | yes     |
| alert_metric_name         | Name of the matric.   | `string`    | `Transactions`   | no      |
| alert_aggregation         | Provide the aggregation value as per requirnment.            | `string`    | `Total`   | no      |
| alert_operator         | The operator is a condition used for alert.            | `string`    | `GreaterThan`   | no      |
| alert_threshold         | Define the threshold for the alert.            | `string`    | `50`   | no      |
| emails         | Add the email ids to be used to send the alert.            | `list`    | `[]`   | no      |
| log_name         | The name of the diagnostic setting.           | `string`    | `default`   | no      |
| log_categories         | Provide the log categories else it will fetch and enable all automatically.  | `list`    | `null`   | no      |
| metric_categories         | Provide the metric categories else it will fetch and enable all automatically.         | `list`    | `null`   | no      |
| retention_days         | The number of days to keep diagnostic logs.        | `number`    | `30`   | no      |



<br/>

#### Outputs

| Name                             | Description                                                             |
| -------------------------------- | ----------------------------------------------------------------------- |
| log_alert_resource                  | This provides resource ID where montitoring alert, log or Matrix has been enabled.                                         |
| log_target_location      | This provies the ID of the resource where log/matrix will be stored

---

<br/>

## Testing

The test folder contains simple Terraform code to create azure monitoring. To test, make sure you have the following environment variables set:

- Terraform
  - ARM_CLIENT_ID - The ID of the app service principal account.
  - ARM_CLIENT_SECRET - The password of the app service principal.
  - ARM_SUBSCRIPTION_ID - The subscription ID where you want to deploy the resources.
  - ARM_TENANT_ID - The tenant ID where the subscription is located where resources will be deployed.
- Consul
  - CONSUL_HTTP_TOKEN - The consul token to use to authenticate to consul for key/value data.
- Infoblox
  - INFOBLOX_USERNAME - The Infoblox username used to authenticate to Infoblox.
  - INFOBLOX_PASSWORD - The Infoblox password used to authenticate to Infoblox.
- Inspec
  - AZURE_CLIENT_ID - The ID of the app service principal account.
  - AZURE_CLIENT_SECRET - The password of the app service principal.
  - AZURE_SUBSCRIPTION_ID - The subscription ID where you want to deploy the resources.
  - AZURE_TENANT_ID - The tenant ID where the subscription is located where resources will be deployed.

To run the test, you can run the following make command:

- `make plan` - Used to run `terraform plan` on the test directory. Also does `terraform init`.
- `make apply` - Used to run `terraform apply` on the test directory. Also does `terraform init`.
- `make destroy` - Used to run `terraform destroy` on the test directory. Also does `terraform init`.
- `make validate` - Used to run `terraform validate` on the test directory. Also does `terraform init`.

## License & Authors

**Author:** Product Technology ([dg-bf-it-product-technology@abbott.com](mailto:dg-bf-it-product-technology@abbott.com))

**Copyright:** 2021 Abbott Laboratories

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
