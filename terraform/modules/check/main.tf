####### VARIABLES

#variable "aws_access_key" {}
#variable "aws_secret_key" {}
#variable "private_key_path" {}
#variable "key_name" {
 #       default = "buildserver"
 #       }

#######################
#PROVIDERS
#################################

provider "google" {
credentials = "${file("credential.json")}"
  project     = "buoyant-striker-238018"
#access_key = "${var.aws_access_key}"   #to use variable
#secret_key = "${var.aws_secret_key}"
zone	= "asia-south1-c"
region  = "asia-south1-c"
}
##############
# RESOURCES
#############

resource "google_compute_instance" "clone-winser" {
 name         = "clone-winse"
  machine_type = "n1-standard-1"
  tags = ["database"]
  #zone         = "asia-south1-c"

    boot_disk {
    initialize_params {
      #image = "debian-cloud/debian-9"
      image = "winser-1"
    }
    }
   // Local SSD disk
  #scratch_disk {}

  network_interface {
    network = "default"
  }
    

}