### Null resource to run command

resource "null_resource" "cluster" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", aws_instance.cluster.*.id)}"
  }

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host = "${element(aws_instance.cluster.*.public_ip, 0)}"
  }

  provisioner "remote-exec" {
    # Bootstrap script called with private_ip of each node in the cluster
    inline = [
      "bootstrap-cluster.sh ${join(" ", aws_instance.cluster.*.private_ip)}",
    ]
  }
}

resource "null_resource" "project_mgmt" {
  triggers = {
    project_id = google_project.my_project.project_id
  }

  provisioner "local-exec" {
    command = <<-EOC
gcloud pubsub topics publish projects/project_mgmt/topics/register --message '{"project":"${self.triggers.project_id}"}'
EOC
  }
  
  provisioner "local-exec" {
    when    = destroy
    command = <<-EOD
gcloud pubsub topics publish projects/project_mgmt/topics/unregister --message '{"project":"${self.triggers.project_id}"}'
EOD
  }
}


resource "null_resource" "nullresource" {
  count = "${var.instance_count}"
  connection {
    type     = "winrm"
    host     = "${element(aws_instance.ec2instance.*.private_ip, count.index)}"
    user     = "${var.username}"
    password = "${var.admin_password}"
    timeout  = "10m"
  }
   provisioner "remote-exec" {
     inline = [
       "powershell.exe Write-Host Instance_No=${count.index}"
     ]
   }
//   provisioner "local-exec" {
//     command = "powershell.exe Write-Host Instance_No=${count.index}"
//   }
//   provisioner "file" {
//       source      = "testscript"
//       destination = "D:/testscript"
//   }
}


resource "aws_elastic_beanstalk_environment" "tfenvtest" {
  name                = "tf-test-name"
  application         = "${aws_elastic_beanstalk_application.tftest.name}"
  solution_stack_name = "64bit Amazon Linux 2018.03 v2.11.4 running Go 1.12.6"

  dynamic "setting" {
    for_each = var.settings
    content {
      namespace = setting.value["namespace"]
      name = setting.value["name"]
      value = setting.value["value"]
    }
  }
}



//log.value.retention_days != null ? true : false
//dynamic is just like for loop

//deinfine name "${var.name}-gen-vnet${local.region_map[var.location]}1"

//######################


 # Copies the myapp.conf file to /etc/myapp.conf
  provisioner "file" {
    source      = "conf/myapp.conf"
    destination = "/etc/myapp.conf"
  }
 # to display the value 
output "db_password" {
  value       = "aws_db_instance.db.password"
  description = "The password for logging in to the database."
  sensitive   = false # yes will hide the value 
   }


# define variable  
variable "filename" { value = "tmp"}

#create file using local_file resource 
variable "filename" {}
variable "content" {}
resource "local_file" "foo" {
    content     = "${var.content}"
    filename = "${var.filename}"
}

# Copies the file as the Administrator user using WinRM
provisioner "file" {
  source      = "conf/myapp.conf"
  destination = "C:/App/myapp.conf"

  connection {
    type     = "winrm"
    user     = "Administrator"
    password = "${var.admin_password}"
  }
}


provisioner "remote-exec" {
    inline = [
      "sudo yum install apache2 -y",
      "service httpd start",
    ]
  }



  ######## use data source ### 

  data "google_compute_instance" "store" {
    Instance_Id = "1996014155251581057"
  }

### Data using filter 

 data "google_compute_instance" "store" {
    filter {
      name = "image-id"
      value = ["ami-c58c1dd3"]
    }
  }


  # now all the attribute has been stored under store ....to use this 

  output "Public_dns_of_instance" {
    value = "${data.google_compute_instance.store.public_ip}"
  }


  #http://aws-cloud.guru/terraform-enterprise-vs-open-source/

#Where Backends are Used
#Backend configuration is only used by Terraform CLI. Terraform Cloud and Terraform Enterprise always use their own state storage when performing Terraform runs, so they ignore any backend block in the configuration.

// Terraform stores tocken at 
// /Users/piyush/.terraform.d/credentials.tfrc.json