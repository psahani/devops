 <#
.SYNOPSIS
  This script creates OUs structure for a given client.
.DESCRIPTION
  This script 
  First: Executes "Import-Module activedirectory" to ensure the required module is installed. 
  Second: Checks the parent OUs with following names: access, accounts & servers, if exists then skips else creates.
  Third: Creates OU for a client under access accounts & server 
  Fourth: Creates below OUs under the client's OU. 
      access = dev/prod/qa/stage
      accounts = roles/users
      servers = dev/prod/qa/stage

.PARAMETER <ClientOuName>
    Only provide the ClientOuName when the script asks while executing.

    Note: The variables values are coded as per standard structure under the variable section, this can be changed as per requirnment. 

.OUTPUTS
  It shares the Created result
.NOTES
  Version:        1.0
  Author:         Sathya
  Creation Date:  10-Oct-21
  Purpose/Change: Initial script development
  
.EXAMPLE
  .\create_ou.ps1
  Or
  .\create_ou.ps1 -ClientOuName <OU_name>
#>

Param (
   [Parameter(Mandatory=$true)]
   [string] $ClientOuName
)

### Variable Section #######

$dc = "DC=ad,DC=merlin,DC=net"
$AccessOuNames = ("dev","prod", "qa", "stage")
$AccountsOuNames = ("roles","users")
$ServersOuNames = ("dev","prod", "qa", "stage")
$ParentOUs = ("access", "accounts", "servers")

#Import active directory module for running AD cmdlets

Import-Module activedirectory

############ Check Parent OUs if exist else create ########

foreach ($ParentOU in  $ParentOUs ) {
if ((Get-ADOrganizationalUnit -LDAPFilter '(name=*)' -SearchBase $dc -SearchScope OneLevel).name -eq "$ParentOU") {
  Write-Host "$ParentOU already exists." -ForegroundColor Green
} else {
  New-ADOrganizationalUnit -Name $ParentOU -Path $dc
  Write-Host "Created Parent New OU = $ParentOU" -ForegroundColor Yellow
}
}

############ Create Client OU ########

foreach ($ParentOU in  $ParentOUs )
{
New-ADOrganizationalUnit -Name "$ClientOuName" -Path "OU=$ParentOU,$dc"
Write-Host "Created New OU = $ClientOuName under OU=$ParentOU,$dc" -ForegroundColor Yellow
}


######### Create OU structure for clients ##########

foreach ($AccessOuName in $AccessOuNames)
{
New-ADOrganizationalUnit -Name "$AccessOuName" -Path "OU=$ClientOuName,OU=access,$dc"
Write-Host "Created New OU = $AccessOuName under OU=$ClientOuName,OU=access,$dc" -ForegroundColor Yellow
}

foreach ($AccountsOuName in $AccountsOuNames)
{
New-ADOrganizationalUnit -Name "$AccountsOuName" -Path "OU=$ClientOuName,OU=accounts,$dc"
Write-Host "Created New OU = $AccountsOuName under OU=$ClientOuName,OU=accounts,$dc" -ForegroundColor Yellow
}

foreach ($ServersOuName in $ServersOuNames)
{
New-ADOrganizationalUnit -Name "$ServersOuName" -Path "OU=$ClientOuName,OU=servers,$dc"
Write-Host "Created New OU = $ServersOuName under OU=$ClientOuName,OU=servers,$dc" -ForegroundColor Yellow
}
