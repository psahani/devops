pipeline {
    agent any 
    
        environment {
        TOKEN = 'sTw02MK00v3eNA.atlasv1.4yCw1C2h4DK386w1bhEn82GCN0UxmhngzLdzvHT72ah8gVjdlKrzTA9XQv1lbaBXNj4'
        WORKSPACE_ID = 'ws-RwXTQB885UYBNKz7'
    }
    stages {
        stage ("scm") {
            steps {
            sh "git clone https://psahani@bitbucket.org/psahani/devops.git"
            }
            }
    
    
        stage ("get data") {
            steps {
            sh label: 'Get state date', script: """\
                    curl -o state.tfstate \
                         --header "Authorization: Bearer "$TOKEN"" \
                         --header "Content-Type: application/vnd.api+json" \
                         "https://app.terraform.io/api/v2/workspaces/"$WORKSPACE_ID"/current-state-version" | jq -r '.data | .attributes | ."hosted-state-download-url"' 
                """ 
            }
        }
    }
}