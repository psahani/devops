pipeline {
    agent any
    tools {
        terraform 'terraform'
    }
 


stages {
        stage ("scm") {
            steps {
            sh "git clone https://psahani@bitbucket.org/psahani/devops.git"
            }
            }
            
        stage ("create credentail file") {
            steps {
                writeFile(file: '/Users/piyush/.terraform.d/credentials.tfrc.json', text: '{ "credentials": { "app.terraform.io": { "token": "sTw02MK00v3eNA.atlasv1.4yCw1C2h4DK386w1bhEn82GCN0UxmhngzLdzvHT72ah8gVjdlKrzTA9XQv1lbaBXNj4" } } }')
            }
        }
        
        stage ("init") {
            steps {
                dir ("devops/terraform/modules/azvm") {
                sh "terraform init"
                }
            }
        }
        
        stage ("plan") {
            steps {
                dir ("devops/terraform/modules/azvm") {
                sh "terraform plan -input=false"
            }
            }
        }

        stage ("apply") {
            input {
                        message 'deploy?'
                        ok 'do it'
                    }
            steps {
                dir ("devops/terraform/modules/azvm") {
                sh "terraform apply -input=false"
            }
            }
        }
        
        
}
 post { 
        always { 
            cleanWs()
        }
    }
}