pipeline {
    agent {
        label '!windows'
    }
  
  // set trigger 
    triggers {
        cron('H 4\* 0 0 1-5')
    }
    //Environment variables can be set globally
    environment {
        DISABLE_AUTH = 'true'
        DB_ENGINE    = 'sqlite'
        PATH = "$PATH:/opt/apache-maven-3.8.2/bin" ///usr/bin/git
    }

    //set parameters 

      parameters {
       string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')
       text(name: 'BIOGRAPHY', defaultValue: '', description: 'Enter some information about the person')
       booleanParam(name: 'TOGGLE', defaultValue: true, description: 'Toggle this value')
       choice(name: 'CHOICE', choices: ['One', 'Two', 'Three'], description: 'Pick something')
       password(name: 'PASSWORD', defaultValue: 'SECRET', description: 'Enter a password')
   }

    parameters {
                            choice(
                                choices: ['ONE', 'TWO'], 
                                name: 'PARAMETER_01'
                            )
    } 
    parameters {
                            booleanParam(
                                defaultValue: true, 
                                description: '', 
                                name: 'BOOLEAN'
                            )
    }
    parameters {
                            text(
                                defaultValue: '''
                                this is a multi-line 
                                string parameter example
                                ''', 
                                 name: 'MULTI-LINE-STRING'
                            )
    }
    parameters {
                            string(
                                defaultValue: 'scriptcrunch', 
                                name: 'STRING-PARAMETER', 
                                trim: true
                            )
    }

//define mulitple steps 
    stages {
        stage('Build') {
            steps {
                echo "Database engine is ${DB_ENGINE}"
                echo "DISABLE_AUTH is ${DISABLE_AUTH}"
                sh 'printenv'
            }
        }
    }
}

//define variable before/after pipeline 
def newvar = "this is value" 

//Or define under script block
stage('Example') {
steps {
    echo 'Hello World'
    script {
        def browsers = ['chrome', 'firefox']
        for (int i = 0; i < browsers.size(); ++i) {
                    echo "Testing the ${browsers[i]} browser"
             }
        }  
    }
}

//define array variable
def lists = ['first', 'second', 'third']


//join string
def first = "piush"
def last = "kumar"
def fullname = "$first $last"
def jobname = "${JOB_NAME}-${fullname}"

//Use filename as jobname_datetime
def now = new Date()
def format = now.format("yyyMMddHHmm")
def filename = "playbook_${JOB_NAME}_${format}"
println filename
stage {
    steps{
        writeFile file: "${filename}.yml", text: 'this is content or variable'
    }
}

//single line string vs multiline string 
"my name is $name"
'''
my name is 
"$name"
'''


// When condition 
pipeline {
    agent any
    stages {
        stage('Example Deploy') {
            when {
                branch 'production'
                //or
                expression { params.REQUESTED_ACTION == 'greeting'}
            }
                echo 'Deploying'
            }
        }
    }
}

// If condition 

stage ('run as per if condition') {
    steps {
        script {
            if (env.BRANCH_NAME == 'master') {
                echo 'i only execute on the master'
            }
            else {
                echo 'i execute elsewhere'
            }
        }

    }
}


//Parallel build stage 

stage ('run parellel') {
    parallel {
        stage ( 'test on windows' ) {
            steps {
                bat "run-tests.bat"    
            }
        stage ('run on linux' ){
            steps {
             sh "run-tests.sh"
            }
        }
        }
    }
}

//user input
stage {
    input {
        message 'deploy?'
        ok 'do it'
        parameters {
            string(name: 'target_enviornment', defaultValue: 'prod', description: 'target deployment enviournment')
        }
    }
    steps {
            echo "deploy release to target env ${target_enviornment}"
    }
}
//container 
stage('Run Playbook'){
      steps {
        container("ansible-2-9-py2")
        sh "ls"
      }
}
//script
  stage('Create playbook & inventory') {
           steps {
               script {
                   sh "ls -l"
                   sh "cat adhoc_inventory.yml"
                   sh "cat adhoc_playbook.yml"
               }

           }
  }
//git
  stage('SCM Checkout') {
        steps {
            git (
            url: 'https://bitbucket.fis.dev/scm/acbsdevops/acbs-ansible.git',
            branch: 'master',
            credentialsId: 'fnfis_piyush_id_dev'
            )
        }
      }

//post zip the file 
post {
    success {
        archiveArtifacts 'testresult.txt'
    }
}

// Post send notification
post {
  failure {
    mail to: 'team@test.com',
      subject: "Pipeline has failed: ${currentBuild.fullDisplayName}",
      body: "Error in ${env.BUILD_URL}"
  }
}


//create file 

   stage('Create playbook & inventory') {
           steps {
                   writeFile file: 'adhoc_inventory.yml', text: 'this is content or variable'
           }
   }

//credential 

stage('use cred'){
    steps {
        withCredentials([usernamePassword(credentialsId: "${credentialsId}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        }
        //or
        withCredentials([string(credentialsId: 'api_token', variable: 'API_KEY')])
    }
}

//change directory 
stage('change dir'){
    steps {
        dir ("/usr/local") {
        sh "cat abc.sh"
        }
        }
    }
}
//clean dir use below code post stages 

 post { 
        always { 
            cleanWs()
        }
    }

//multiline script

steps {
sh label: 'this is script', script: """\

"""
}